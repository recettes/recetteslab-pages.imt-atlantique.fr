[https://recettes.gitlab-pages.imt-atlantique.fr/](https://recettes.gitlab-pages.imt-atlantique.fr/)

# Fonctionnement

Le site est généré à partir de fichiers markdown en utilisant l'outil mkdocs et le thème Material via la CI/CD de gitlab. Gitlab s'occupe de générer et publier le site, aucune opération manuelle est nécessaire pour modifier le contenu d'une page.

La CI/CD est configurée dans le fichier yaml `.gitlab-ci.yml`

# Les pages

Les pages sont générées à partir de fichiers markdown stockés dans le répertoire `docs`.

Les images qui apparaissent dans les pages sont stockées dans le répertoire `docs/img` et doivent être nommées avec la forme `nom-de-la-page_titre-image`. Par exemple `reMarkable-06-enregistrer.png`.

Les fichiers sont stockés dans le répertoire `docs/files`.

Les logos, les CSS additionnels sont dans `docs/assets`

# La configuration du site

La configuration du site est contenu dans le fichier `mkdocs.yml`. C'est dans ce fichier que l'on peut modifier le menu, sous la section `nav:`

# Documentation utiles

- Mkdocs : [https://www.mkdocs.org/user-guide/writing-your-docs/](https://www.mkdocs.org/user-guide/writing-your-docs/)
- Thème Material : [https://squidfunk.github.io/mkdocs-material/](https://squidfunk.github.io/mkdocs-material/)
