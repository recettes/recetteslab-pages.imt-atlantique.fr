# How to make Chinese Dumplings, Luxi Style

## The pastry

Put a quantity of plain flour in a bowl and make a well in the middle. Gradually add small quantities of water, mixing each time, until the dough holds together (not too wet, so the dry pieces still falling off). When it forms one piece, remove from the bowl and kneed on a floured surface until smooth. Put to one side in a bowl and cover with a damp tea-towel while making filling

## The filling (pork with cabbage)

Take a quantity of finely minced pork and put in a large bowl. Finely chop chinese cabbage, put in another bowl, douse with salt and squeeze out the liquid by hand, remove the squeezed cabbage and add to the minced pork. Add very finely chopped ginger. Add very finely chopped spring onion. Quantities (to taste) of light soy sauce, rice wine, sesame oil, sichuan pepper powder, white pepper, salt, a pinch of sugar, etc. (not necessarily all of those). Teaspoon of corn starch. Mushrooms (lightly fried with ginger, light soy sauce and sesame oil) - optionalAdd all to the mixing bowl (if the filling is too dry, add a bit water or the cabbage juice) and stir, in one direction, for several minutes until all well mixed together and is a smooth, slightly bouncy, consistency


## The filling (vegetarian)

Finely chop chinese cabbage, put in a bowl, douse with salt and squeeze out the liquid by hand, remove the squeezed cabbage. Finely chop the dry tofu and mushrooms, mix them together in a bowl. Add a table spoon of cooking oil in the wok, lightly fried the mixture with ginger, add a bit light soy sauce and sesame oil. Scramble an egg. Fry the egg into a thin sheet. Finely chop it. Finely chop spring onion. Quantities (to taste) of light soy sauce, sesame oil, sichuan pepper powder, white pepper, salt, a pinch of sugar, etc. (not necessarily all of those). Add all to the mixing bowl and stir for several minutes until all well mixed together.


## Assembling the dumplings

Remove the dough from the bowl and kneed some more. Make the dough into a large ring shape and tear off a section. Roll the section on a floured board until it forms a sausage about 2-3cm in diameter. With a sharp knife, chop the sausage into approx 1-2cm pieces, rotating the sausage 90 degrees each time to ensure a uniform shape. Take a section and flatten it to the board with the palm of your hand to make a small round pancake shape. Take a small rolling pin in your right hand and holding the pancake in your left hand, roll into the centre of the pancake and back again.. Rotate the pancake slightly and repeat. Keep doing this until the pancake has been flatted into a round shape, thin at the edges and thicker towards the centre. The pancake should roughly fit in the palm of your hand. Not too big, not too small. With the pancake in the palm of your hand, put a small amount of filling in the centre. Folding the pancake in half, firmly pinch it at the top. Using various techniques, but always with aesthetics in mind, gather the rest of the edges together and ensure they are firmly sealed to prevent them breaking apart during cooking. Adding folds or neatly bunched tend to be the preferred.Place on a well-floured surface when done to prevent sticking


## If boiling

Fill a wok with enough water to cover the dumplings. Have a jug of cold water standing by. Add a pinch or two of salt, this helps to prevent the dumplings sticking together. When water is boiled, carefully place the dumplings in the water and stir gently to prevent them from sticking together or sticking to the bottom of the wok (especially in the early stages).When it comes to the boil, douse with cold water from the jug. Just enough to stop the water to boil. Allow it to boil over again and repeat.When it boils over the 3rd time, carefully remove the dumplings from the wok and serve. You may wish to drain the excess water (if any) first although most should evaporate naturally.


## If steaming

Place the dumplings in a steamer so none are touching (keep the lowest rack clear if there is a risk of water splashing the dumplings). Place on cabbage leaf or waxed paper (or other) to prevent sticking to the rack.Turn up the heat and set a timer for 10 minutes once the water has started boiling (8 minutes for vegetarian dumpling)Keeping boiling and after 10 minutes remove the racks from the water and serve.


## Dipping sauce

Combine 2 tablespoon of light soy sauce, 1/2 teaspoon of dark rice vinegar, 1/2 teaspoon of sugar, finely chopped garlic, 1/2 tablespoon of sesame oil, sichuan pepper powder (optional), finely chopped spring onion and chill oil together. It should compliment the filling e.g. if the filling is simple, maybe add more flavor and complexity to the dip. If the filling is rich, keep the dip light. For example, in the north of China where people usually make dumplings with rich-flavored fillings, the dipping sauce usually is as simple as just containing vinegar and garlic. Most importantly, make it to taste. This dipping sauce can be used as the dressing of chinese salad, like the dry tofu salad.


## Chili oil

Mix roughly-grounded dry chili pepper powder together with roasted white sesame seedHeat up the cooking oil in the wok. Put a block of smashed ginger, some sichuan pepper corns and a star anise in the hot oil for a few seconds until the oil smells aromatic. Remove them before they get burnt. Turn off the heat. Let the oil cool down for a second. Then pour the hot oil into the chill pepper powder in three times, stir it all the time while pouring. Close the lid. It gets more and more aromatic after some time.
