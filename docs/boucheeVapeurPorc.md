# Bouchée vapeur porc et légumes

[https://www.reddit.com/r/BonneBouffe/comments/1azjg2d/comment/ks1woej/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button](https://www.reddit.com/r/BonneBouffe/comments/1azjg2d/comment/ks1woej/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button)

## Ingrédients

### Pâte

- 500 g de farine (prévoir plus pour les pâtons)
- 30 cl d'eau
- 6g de sel

### Farce

- 1 demi chou
- 4 grosses carottes
- 4 champignons
- 1 grosse poignée de cacahuètes
- 500 grammes de chair à saucisses
- 1 grosses cuillère à soupe de sauce soja
- Sel
- Poivre
- Oignon en poudre

## Préparation

1. Préparer la pâte dans un bol et la laisser poser une heure au frais sous un film plastique
2. Couper grossièrement les légumes et les passer au blender pour avoir un haché Mélanger avec la chair à saucisse puis reserver.
3. Rouler la pâte sur plan (très fariné) et avec un verre ou un emporte pièce faire des ronds.
4. On prend un cercle de pâte, on met une cuillère à café de farce et on roule en boule jusqu'à épuisement de la pâte.
5. Pour la cuisson on fait chauffer un peu d'huile dans une poêle, on ajoute les boules deux minutes et on verse un fond d'eau dans la poêle (mi hauteur des vouels) cuire 5 minutes. Et recommencer l'étape jusqu'à ce que toute la fournée soit cuite.


À déguster avec un peu de sauce soja ou sweet chili. Bon appétit.
