# New-York style Pizza

## Résumé

source : [https://www.youtube.com/watch?v=lzAk5wAImFQ](https://www.youtube.com/watch?v=lzAk5wAImFQ)

Préchauffer le four à 290° avec la grille au milieu (si pierre à pizza, la faire préchauffer une heure)

## Ingrédients

- CaC de levure
- CaC de sucre
- 60ml d’eau tiède
- 480ml d’eau tiède
- CaS de sucre
- CaS de sel
- 50ml d’huile d’olive
- 640 de farine

## Préparation

1.	Mélanger la levure, une CaC de sucre, et 60ml d’eau tiède. Attendre 5 bonnes minutes
2.	Ajouter 480ml d’eau tiède, une CaS de sucre, une de sel, 50ml d’huile d’olive et 640g de farine
3.	Pétrir jusqu’à avoir une pâte souple qui s’étire jusqu’à être translucide, et quasi non collante. Ajouter de la farine si nécessaire.
4.	Séparer la pâte en 4 boules.
5.	Dans 4 récipients, mettre un fond d’huile, y déposer un morceau de pâte dans chaque, huiler la pâte et s’en servir pour huiler les récipients.Fermer les récipients et les mettre au frigo. Laisser pousser au moins 24h, 48h si possible.
6.	Étirer la pâte à la main sans la presser.
7.	Idéalement la mettre sur un pelle à pizza légèrement recouverte de farine pour la garnir
8.	La déposer sur la pierre à pizza préchauffée.
9.	5 minute de cuisson, au jugé...
