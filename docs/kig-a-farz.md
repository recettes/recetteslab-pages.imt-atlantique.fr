# Kig a Farz (Bruzig) recette micro onde.

## Ingrédients :

- 250g de farine de blé noir
- 65 grammes de beurre
- 10cl de crème fraiche
- 40cl de lait
- 2 oeufs
- 1 cuillère à café de sel

## Préparation

(Mélanger tous les ingrédients à la cuillère en bois)

- Délayer d'abord farine et sel avec une partie de lait
- Ajouter le beurre ramolli en pommade (important : ne pas le faire fondre)
- Battre les oeufs en omelette et les incorporer à la pâte, puis la crème  et le reste du lait
- Faire cuire au micro onde 8 minutes
- Émietter à la fourchette puis remettre pour 2 minutes
