# Banana Bread

## Résumé

- (recette d’Anna, originellement en cup)
- Préchauffer le four à 180° avec la grille au milieu
- Cuisson 1 heure

## Ingrédients

- 120g de beurre
- 300g de sucre
- 2 œufs
- 250g de farine
- 1 CàS de sucre vanillé
- $1/2$ CàC de sel
- $1/2$ CàS de bicarbonate de soude
- 60 ml de lait
- 3 bananes bien mûres

## Préparation

1.	Faire fondre le beurre
2.	Mélanger avec le sucre, le sucre vanillé et les œufs, réserver
3.	Mélanger la farine, le sel, le bicarbonate
4.	Ajouter le mélange beurre sucres, mélanger le tout
5.	Écraser 2 bananes, les ajouter
6.	Ajouter le lait, bien mélanger
7.	Verser dans un moule à cake
8.	Couper la 3em banane dans la longueur, poser les 2 moitiés sur le dessus
9.	Enfourner une heure à 180°
