# Far

Recette donnée par Amélie, infirmière à Locmaria.

Préchauffer le four à 180°C

## Ingrédients

- 3 oeufs
- 75g de sucre
- un sachet de sucre vanillé
- 330g de lait
- 25g de beurre
- au choix :

    * speculoos émiettés
    * Chocolat
    * pommes
    * raisins
    * pruneaux
    * cacahuètes

## Préparation

Tout mélanger, mettre dans un plat au four 30 minutes à 180°C
