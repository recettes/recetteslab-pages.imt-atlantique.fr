# Crème de châtaignes

## Ingrédients

- 2,4kg de châtaignes
- 2kg de sucre
- $1/2$ cuillère à café de poudre de vanille
- 3 verres d'eau

## Préparation

1. Inciser le cul des châtaignes en croix
2. Les faire bouillir 10 minutes, les éplucher, les remettre à cuire 15 minutes
3. Passer les châtaignes au moulin à purée
4. Verser le sucre et l'eau dans une grande marmite et mettre à chauffer
5. Faire un sirop et attendre que ça bout et que le mélange monte
6. Verser la purée de châtaigne dans le sirop et mélanger sur feu vif jusqu'à ce que ce soit épaissit
7. Verser dans des pots de confiture passés à l'eau bouillante
8. Fermer les pots et les retourner
