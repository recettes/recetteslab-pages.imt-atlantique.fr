Les crêpes de Raymond Oliver

## Résumé

[https://www.youtube.com/watch?v=YbxWMDdVSPY](https://www.youtube.com/watch?v=YbxWMDdVSPY)

## Ingrédients

- Vanille
- 2 grosse pincée de sel
- 4 cuillère à soupe de sucre
- $1/4$ de litre de lait
- 400 grammes de farine
- 2 cuillères à soupe d’huile
- 8 œufs
- 100g de beurre
- 50ml de pastis
- $1/4$ litre de rhum
- Allongée à la bière


## Préparation

1. Dans une casserole faire chauffer le lait, le beurre, le sel et le sucre
2. Dans un saladier, mélanger la farine, l’huile et les œufs. Mélanger quelque minutes. Puis ajouter le lait.
3. Ajouter un soupçons de pastis puis le rhum. Mélanger. Ajouter de la bière pour que la pâte soit très liquide.
