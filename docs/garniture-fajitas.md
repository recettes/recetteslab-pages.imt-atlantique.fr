# Garniture Fajitas maison

## Ingrédients

- 2 gros oignons
- 1 grosse carotte
- 400g de haricots rouge
- 700g de viande hâchée
- 1 petite boite de maïs
- 1 briquette de crème fraîche
- 1 CàS de paprika
- poivre : au jugé
- 3 CàS d'huile d'olive ou +
- herbes de Provence
- un verre de vin rouge
- 1~3 CàS de cacao

# Préparation

- Faire revenir les oignons émincés à l'huile d'olive à feu moyen avec le paprika et le poivre
- Une fois ces derniers bien dorés, ajouter la viande, les herbes de Provence, et le vin
- Ajouter les carottes râpées
- Passer à feu fort
- Repasser à feu moyen quand la viande est cuite et ajouter le maïs, les haricots, le cacao et la crème
- Remuer et faire réduire

Sur les tortillas la garniture accompagnée de fromage râpé, de crème fraîche et de sauce pimentée
