# Cookies

Source : [https://lauratodd.com/pages/recette-cookies-chocolat-lait](https://lauratodd.com/pages/recette-cookies-chocolat-lait)

## Ingrédients

- 250 g de beurre
- 1 œuf
- 170 g de cassonade
- 170 g de vergeoise blonde (sinon cassonade)
- 400 g de farine de blé type t55
- 425 g de pépites de chocolat
- 1 sachet de levure sans phosphate
- des noisettes/amandes/etc
- poudre d'amande
- 2 pincées de sel

## Préparation

1. Pesez tous les ingrédients scrupuleusement et préparez votre matériel.
2. Dans un grand récipient tout rond, déposez le beurre ramolli (il doit avoir la consistance d’une pommade).
3. Versez ensuite le sucre dessus et mélangez le tout à l’aide d’une spatule.
4. Ajoutez l'œuf et mélangez de nouveau l’ensemble (l’œuf doit être complètement incorporé à la préparation).
5. À part, tamisez les poudres : la farine, la levure.
6. Incorporez les poudres au premier mélange (beurre, sucre, œuf).
7. Ajoutez la fleur de sel, les pépites de chocolat ( et éventuels autres ingrédients pour les plus gourmands) et poursuivre le mélange, jusqu'à ce que le tout soit joliment homogène.
8. Pour former les cookies, munissez-vous d'une cuillère à glace, remplissez-la de pâte, puis former le cookie. Le cookie doit peser 50g à peu près. Aplatissez légèrement chaque cookie et conservez au frais pendant 2h minimum.
9. Préchauffez votre four à température moyenne (140° ou thermostat 4-5). Chaud devant !
10. Disposez vos boules de pâte sur une plaque de cuisson anti adhésive ou recouverte d’une feuille de papier cuisson.
11. Il ne vous reste plus qu'à enfourner vos cookies pendant 15 minutes.
12. Après cuisson, l'idéal est d'attendre une quinzaine de minutes avant dégustation, vos cookies seront alors tièdes et parfaitement délicieux. Il ne vous reste plus qu'à déguster vos cookies maison.
