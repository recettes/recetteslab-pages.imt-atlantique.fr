# Focaccia Gênoise

[https://www.youtube.com/watch?v=nChT0wXeMn4](https://www.youtube.com/watch?v=nChT0wXeMn4)

[https://www.cuisineaddict.com/achat-moule-a-genoise-evase-40-x-30-cm-h-35-cm-fer-blanc-27627.htm](https://www.cuisineaddict.com/achat-moule-a-genoise-evase-40-x-30-cm-h-35-cm-fer-blanc-27627.htm)

## Résumé

- Cuisson : 15 minute à 220° sans chaleur tournante
- Préparation : 15 minutes
- Temps de pousse total (sous linge) : 3h10 (20 + 30 + 30 +2h) autour de 25°

## Ingrédients

- 500g de farine de gruau (à pâtisserie)
- 10g de levure fraîche de boulanger
- 12g de sel fin
- 1 CAC de sucre en poudre
- 30cl d’eau
- 30g d’huile d’olive
- 5cl d’eau
- 5cl d’huile d’olive

## Préparation

1. Mettre la **farine** dans un grand saladier, faire un puits.
2. Dans les **30cl d’eau** mélanger la **levure** de boulanger, et **verser** une partie de l’eau sur la farine
3. Ajouter la **CAC de sucre** et les **30g d’huile d’olive**.
4. **Pétrir** une bonne minute avec le bout des doigts en ramenant la farine vers le centre. La pâte reste collante.
5. Ajouter les **12g de sel**
6. **Pétrir** quelques minute pour faire une **boule**.
7. Faire **pousser pendant 20 min à 25°-30°** dans le saladier recouvert d’un torchon
8. Généreusement et uniformément **huiler le plat** de cuisson et **déposer la pâte**.
9. Refaire **pousser 30 min**
10. **Étaler** soigneusement la pâte dans tout le plat.
11. Laisser encore **pousser 20 min**
12. Préparer une **saumure** avec les **5cl d’eau**, les **5cl d’huile d’olive** et une **pincé de sel**
13. **Trouer** la focaccia un peu partout avec le bout des doigts
14. Recouvrir de la saumure
15. Refaire **pousser 2 heures**
16. Saupoudrer de **gros sel**
17. **Préchauffer à 220°** sans chaleur tournante (risque de dessécher sinon)
18. **Cuire** à mi-hauteur **15 min**
