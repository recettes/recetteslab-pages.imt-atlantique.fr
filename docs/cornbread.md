# Cornbread

## Résumé

(recette d’Anna, originellement en cup)
Préchauffer le four à 220° avec la grille au milieu
Cuisson 22 minutes

## Ingrédients

- 57g de beurre
- 1 œuf
- 236ml de lait
- 60ml de crème ou de yaourt nature
- 50g de sucre
- 150g de farine de maïs
- 120g de farine
- 1 CAS de poudre à lever
- $1/8$ de CAC de bicarbonate
- $1/2$ CAC de sel

## Préparation

1. Beurrer le plat, saupoudrer de la farine dessus, évacuer le surplus
2. Faire fonde le beurre
3. Ajouter le lait, la crème ou le yaourt et le sucre. Mélanger
4. Ajouter la farine de maïs, la farine, la poudre à lever, le bicarbonate et le sel. Mélanger
5. Verser dans le plat et mettre au four 22 minutes


## Recette originelle

![Cornbread page 1](./img/cornbread/cb1.jpg)
![Cornbread page 2](./img/cornbread/cb2.jpg)
