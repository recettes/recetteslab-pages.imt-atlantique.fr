# Foie de veau

Merci à Sebastien Blot du RAK : [https://services.imt-atlantique.fr/rak/](https://services.imt-atlantique.fr/rak/)

Fariner le foie de veau et le poêler.

Déglacer au vinaigre de vin, faire suer l'oignon rouge émincé, ajouter les câpres, du beurre et fond de volaille.
